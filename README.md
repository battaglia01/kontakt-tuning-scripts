These are a bunch of miscellaneous tuning scripts for Kontakt.

"Kontakt Microtuning Pianoteq-Style Channel Offset.nkp" is a Kontakt script that lets you tune your instrument to any EDO, and also follows the Pianoteq style method of having Ch. 2 an octave higher than Ch. 1, Ch 3. two octaves higher, and Ch. 16 one octave lower, and so on, which is also used in many of the standard Lumatone presets. This is based on the original built-in Kontakt microtuning script and I simply modified it to add the channel support.

"Kontakt Microtuning Pianoteq-Style Channel Offset.txt" is the same thing but in plain text form, in case it's easier for you to import it that way.

"Tonewheel Organ B3 Mike Modified Tunable.nki" is a modified version of Kontakt's "Vintage Organs" B3 instrument in which the drawbars are individually microtunable. Underneath each drawbar there are little textboxes which let you adjust the tuning of each drawbar. Note that the default for this, both in Kontakt's library and on a real B3, is that the drawbars are tuned to match the tuning of 12-EDO (!) rather than the usual harmonic series - the "1 3/5" drawbar, for instance, which is basically the 5th harmonic, is actually tuned to 2800 cents rather than 2786 cents. If you adjust the value for this drawbar to -14 cents, you will tune the drawbar to 5/1 instead, which is pretty useful for 31-EDO. You can adjust all of the drawbars this way. Note that you need the Vintage Organs sample library for this to work.

I don't really intend to update this thing that much and have only tested it with Kontakt 6, so if you see any bugs, you may want to do some debugging of your own!
